package com.sauce.magmonitor.entity;

public class RequestStatus {

	private String rawResponse;
	
	private String url;
	
	private Long executionTime;
	
	public RequestStatus(String rawResponse, String url, Long executionTime) {
		this.rawResponse = rawResponse;
		this.url = url;
		this.executionTime = executionTime;
	}

	public Long getExecutionTime() {
		return executionTime;
	}
	
	public boolean isOk() {
		return rawResponse.equals("Magnificent!");
	}
	
}
