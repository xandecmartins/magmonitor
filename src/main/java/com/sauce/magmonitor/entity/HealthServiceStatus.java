package com.sauce.magmonitor.entity;

public class HealthServiceStatus {

	private float healthyPercent;
	private int amountRequests;
	private float averageExecutionTime;

	private float totalExecutionTime;
	private float quantityHealth;

	public HealthServiceStatus() {
	}

	public float getHealthyPercent() {
		return healthyPercent;
	}

	public int getAmountRequests() {
		return amountRequests;
	}

	public float getAverageExecutionTime() {
		return averageExecutionTime;
	}

	public void addNewRequest(RequestStatus requestStatus) {

		amountRequests++;
		totalExecutionTime += requestStatus.getExecutionTime();

		averageExecutionTime = ((float) totalExecutionTime / amountRequests);
		
		if (requestStatus.isOk())
			quantityHealth++;

		healthyPercent = (quantityHealth / amountRequests) * 100;
	}

	public String toString() {
		return String.format("%s %.2f; %s %.2f %s", "Requests OK: ", healthyPercent, "Average Execution Time: ",
				averageExecutionTime, "ms");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amountRequests;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HealthServiceStatus other = (HealthServiceStatus) obj;
		if (amountRequests != other.amountRequests)
			return false;
		return true;
	}
}
