package com.sauce.magmonitor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sauce.magmonitor.entity.HealthServiceStatus;
import com.sauce.magmonitor.service.MonitorService;

@RestController
public class MonitorController {

	public static final Logger logger = LoggerFactory
			.getLogger(MonitorController.class);
	
	@Autowired
	private MonitorService serviceMonitor;
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public ResponseEntity<?> getStatus() {
		logger.debug("Receiving request");
		return new ResponseEntity<HealthServiceStatus>(serviceMonitor.getStatus(),
				HttpStatus.OK);
	}
	
}
