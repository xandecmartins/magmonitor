package com.sauce.magmonitor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.sauce.magmonitor.service.MonitorService;

@Component
public class ServerInitializer implements ApplicationRunner {

	@Autowired
	private MonitorService serviceMonitor;
	
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
    	serviceMonitor.start();
    }
}