package com.sauce.magmonitor.service;

import com.sauce.magmonitor.entity.HealthServiceStatus;

public interface MonitorService {

	void start();
	
	HealthServiceStatus getStatus();
}
