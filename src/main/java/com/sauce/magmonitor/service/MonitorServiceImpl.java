package com.sauce.magmonitor.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.sauce.magmonitor.AppProperties;
import com.sauce.magmonitor.entity.HealthServiceStatus;
import com.sauce.magmonitor.entity.RequestStatus;

public class MonitorServiceImpl implements MonitorService {

	public static final Logger logger = LoggerFactory.getLogger(MonitorServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AppProperties appProperties;

	private HealthServiceStatus currentStatus;

	@PostConstruct
	public void init() {
		currentStatus = new HealthServiceStatus();
	}

	public void start() {
		new Thread() {
			public void run() {
				while (true) {
					String result = "";
					Long ini = 0L;
					try {
						ini = System.currentTimeMillis();
						result = restTemplate.getForObject(appProperties.getUrl(), String.class);
					} catch (Exception e) {
						handleException(e);
					} finally {
						Long end = System.currentTimeMillis();
						RequestStatus reqStatus = new RequestStatus(result, appProperties.getUrl(), (end - ini));
						currentStatus.addNewRequest(reqStatus);
						logger.info(currentStatus.toString());
					}
					
					waitMonitoring();
				}
			}
		}.start();
	}
	
	public HealthServiceStatus getStatus() {
		return currentStatus;
	}
	
	private void handleException(Exception e) {
		if (e.getMessage().contains("Read timed out")) {
			logger.warn("Timeout during connection with remote server");
		} else if (e.getMessage().contains("Connection refused")){
			logger.warn("Server is unavailble");
		} else {
			logger.warn("Problems during server request: "+e.getMessage());
		}
	}
	
	private void waitMonitoring() {
		try {
			Thread.sleep(appProperties.getWaitTime());
		} catch (InterruptedException e) {
			logger.error("Problems during the wait time",e);
		}
	}

}
