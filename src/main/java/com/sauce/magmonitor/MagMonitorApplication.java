package com.sauce.magmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagMonitorApplication.class, args);
	}
	
	
}
