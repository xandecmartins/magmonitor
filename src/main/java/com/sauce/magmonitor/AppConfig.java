package com.sauce.magmonitor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.sauce.magmonitor.service.MonitorService;
import com.sauce.magmonitor.service.MonitorServiceImpl;


@Configuration
public class AppConfig {
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate(clientHttpRequestFactory());
	}
	
	@Bean
	public MonitorService monitorService() {
		return new MonitorServiceImpl();
	}
	
	private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(5000);
        factory.setConnectTimeout(5000);
        return factory;
    }
	
}