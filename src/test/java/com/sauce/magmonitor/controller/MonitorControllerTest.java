package com.sauce.magmonitor.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.sauce.magmonitor.BuildUtil;
import com.sauce.magmonitor.TestUtils;
import com.sauce.magmonitor.entity.HealthServiceStatus;
import com.sauce.magmonitor.service.MonitorService;

@RunWith(SpringRunner.class)
@WebMvcTest(MonitorController.class)
public class MonitorControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MonitorService monitorService;

	private static final String URL_STATUS = "/status";
	
	@Test
	public void testGetStatus() throws Exception {

		when(monitorService.getStatus()).thenReturn(BuildUtil.buildStatus());

		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.get(URL_STATUS).accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

		int status = result.getResponse().getStatus();
		assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

		verify(monitorService).getStatus();

		HealthServiceStatus resultHealth = TestUtils.jsonToObject(result.getResponse().getContentAsString(), HealthServiceStatus.class);
		assertNotNull(resultHealth);
		assertEquals(BuildUtil.buildStatus().getAmountRequests(), resultHealth.getAmountRequests());
	}
	
}
