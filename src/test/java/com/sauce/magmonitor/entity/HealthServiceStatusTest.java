package com.sauce.magmonitor.entity;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sauce.magmonitor.BuildUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HealthServiceStatusTest {

	
	@Test
	public void testAddNewRequest() {
		HealthServiceStatus status = new HealthServiceStatus();
		status.addNewRequest(BuildUtil.buildRequestStatus());
		
		assertNotEquals(status, new HealthServiceStatus());
	}
}
