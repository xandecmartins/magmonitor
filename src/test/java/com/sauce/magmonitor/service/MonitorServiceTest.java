package com.sauce.magmonitor.service;

import static org.junit.Assert.*;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sauce.magmonitor.AppProperties;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MonitorServiceTest {

	@InjectMocks
	private MonitorServiceImpl monitorServiceImpl;
	
	@Mock
	private AppProperties appProperties;
	
	@Before
	public void init() {
		monitorServiceImpl.init();
	}
	
	@Test
	public void testGetStatus() {
		when(appProperties.getWaitTime()).thenReturn(1000L);
		when(appProperties.getUrl()).thenReturn("http://localhost:12345");
		
		assertNotNull(monitorServiceImpl.getStatus());
	}
}
