package com.sauce.magmonitor;

import com.sauce.magmonitor.entity.HealthServiceStatus;
import com.sauce.magmonitor.entity.RequestStatus;

public class BuildUtil {

	public static HealthServiceStatus buildStatus() {
		HealthServiceStatus status = new HealthServiceStatus();
		status.addNewRequest(buildRequestStatus());
		return status;
	}
	
	public static RequestStatus buildRequestStatus() {
		return new RequestStatus("Magnificent!", "http://localhost:12345", 21L);
	}
	
	public static RequestStatus buildRequestStatusNotOk() {
		return new RequestStatus("500: Internal Server Error", "http://localhost:12345", 11L);
	}
}
