# MagMonitor System
    This project is part of a test for a software engineer position. 
    There is a XML file (Java_Developer_Task.xml) that explains the challenge.

## Architecture
    The project is service that makes a monitoring of a HTTP service, working as a daemon, extracting data and storing in a log file to be monitored. Otherwise, it is a back-end server that provides a REST interface to extract the status the monitored service.
* Stack: 
    * Spring-Boot 1.5.7
    * Java 8
    * TomCat Server(embedded)
## Test Structure
## Unit Test & Coverage
	It was defined, using JaCoCo plugin, the check point of coverage in 65%.
### How to Generate Cover Report
    mvn clean test jacoco:report
    firefox target/site/jacoco/index.html

## How to Run
### Requirements
* Java 8 - JDK
* Maven

### Execution Commands
    mvn package
    java -jar target/magmonitor.jar
* After this, the services could be accessed in your browser.

## Example of use
    http://localhost:8080/magmonitor/status